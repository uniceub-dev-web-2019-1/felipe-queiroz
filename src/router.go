package main

import ( "github.com/gorilla/mux" )

func createRouter () (r *mux.Router){
	r = mux.NewRouter()

	r.HandleFunc("/users", SearchUser).Methods("GET").Queries("name","{name}")

	r.HandleFunc("/users", InsertUser).Methods("POST").Header("Content-type")

	r.HandleFunc("/users", EditUser).Methods("PUT")

	return
}