package main

import (
		"log"
		"net/http"
		"time" 
		)

func StartServer(){

	server := http.Server{Addr: "172.22.51.138:8080",
											Handler: createRouter(),
											ReadTimeout: 100 * time.Millisecond,
											WriteTimeout: 200 * time.Millisecond,
											IdleTimeout: 50 * time.Millisecond}

	//inicializar o servidor
	log.print(server.ListenAndServe())

}